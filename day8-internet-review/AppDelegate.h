
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>


/**
 @description This class was created at UCLA for a demo
 
 I hope it is helpful....
 
 @author Dave Henson
 
 @warning This is just a class demo!
 
 @see http://www.disney.com
 
 
*/
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

