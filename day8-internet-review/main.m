//
//  main.m
//  day8-internet-review
//
//  Created by Dave Henson on 11/7/16.
//  Copyright © 2016 Certified Networks, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
