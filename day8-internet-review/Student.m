//
//  Student.m
//  day8-internet-review
//
//  Created by Dave Henson on 11/7/16.
//  Copyright © 2016 Certified Networks, Inc. All rights reserved.
//

#import "Student.h"

@implementation Student

/** @warning this is not implemented  */
-(void)SendAnEmailTo:(NSString *)recipient withSubject:(NSString *)subject
{
    //todo....
}

/** fullName is calculated */
-(NSString *)fullName
{
    return [NSString stringWithFormat:@"%@ %@", _firstName, _lastName];
}

@end
