//
//  ViewController.m
//  day8-internet-review

#import "ViewController.h"


@implementation ViewController

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //be sure to update info.plist to allow non-ssl connections...
    NSString *urlString = @"https://s3.amazonaws.com/certifiednetworks/students.json";
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    dispatch_queue_t myqueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    
    
    dispatch_async(myqueue, ^{
        NSData *result = [NSData dataWithContentsOfURL:url];
        
        NSString *resultString = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
        
        //update label
        dispatch_async(dispatch_get_main_queue(), ^{
            textView.text = resultString;

        });
        
    });
    
    
}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    Student *s = [[Student alloc] init];
    [s SendAnEmailTo:@"" withSubject:@""];
    

    textView = [[UITextView alloc] initWithFrame:CGRectMake(10.0, 30.0, 300.0, 440.0)];
    [self.view addSubview:textView];
    
    textView.text = @"click the screen to update";
}


@end
