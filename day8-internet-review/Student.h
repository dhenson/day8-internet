
#import <Foundation/Foundation.h>

/**
 This "Student" class is demonstrating comments....
 
 @seealso http://www.certifiednetworks.com
 */
@interface Student : NSObject
{
    int fullname;
}

/** firstName is added to lastName to create full name*/
@property() NSString *firstName;

/** lastName is added to firstName to create full name*/
@property() NSString *lastName;

-(void)SendAnEmailTo:(NSString *)recipient withSubject:(NSString *)subject;

-(NSString *)fullName;



@end
